"""Utilizando diccionarios diseñar un programa modular que permita gestionar los productos de un 
   comercio, las funcionalidades solicitadas son: 
    a. Registrar productos: para cada uno se debe solicitar, código, descripción, precio y stock. 
       Agregar las siguientes validaciones: 
        i. El código no se puede repetir
        ii. Todos los valores son obligatorios
        iii. El precio y el stock no pueden ser negativos
    b. Mostrar el listado de productos
    c. Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]
    d. Diseñar un proceso que le sume X al stock de todos los productos cuyo valor actual de stock 
       sea menor al valor Y.
    e. Eliminar todos los productos cuyo stock sea igual a cero.
    f. Salir"""
#--------------------------------------------------------------------------------------------
import os
#--------------------------------------------------------------------------------------------
def menu():
    os.system('cls')
    print("MENU PRINCIPAL \n" + 
        "1-Registrar Productos\n" +
        "2-Mostrar Listados\n" +     
        "3-Stock dentro de un Intervalo\n" +
        "4-Sumar X a un Stock menos a Y\n" +
        "5-Eliminar Stock (0)\n" +
        "6-Salir")
    opcion = int(input("ELIGA UN OPCION: "))
    while not (opcion >= 1 and opcion <= 6):
        opcion = int(input("ELIGA UN OPCION: "))
    return opcion
#--------------------------------------------------------------------------------------------
def esperar():
    input('Presione una tecla...')
#--------------------------------------------------------------------------------------------
def cargarDescripcion():
    desc = input('Descripcion: ')
    while desc == '':
        print('Campo Incompleto')
        desc = input('Descripcion: ')
    return desc
#--------------------------------------------------------------------------------------------
def cargarPrecio():
    pre = input('Precio: ')
    while pre == '':
        print('Campo Incompleto')
        pre = input('Precio: ')
    pre = float(pre)
    return pre       
#--------------------------------------------------------------------------------------------
def cargarStock():
    st = input('Stock: ')
    while st == '':
        print('Campo Incompleto')
        st = input('Stock: ')
    st = int(st)
    return st
#--------------------------------------------------------------------------------------------
def validar(x):
    if x < 0:
        return True
    else:
        return False
#--------------------------------------------------------------------------------------------
def registrarProductos(productos):
    resp = 's'
    while resp == 's':
        codigo = input('Codigo: ')
        while codigo == '':
            print('Campo Incompleto')
            codigo = input('Codigo: ')
        codigo = int(codigo)       
        if codigo not in productos:
            descripcion = cargarDescripcion()
            presio = cargarPrecio()
            stock = cargarStock()   
            if not(validar(presio)) and not(validar(stock)):
                productos[codigo] = [descripcion,presio,stock]
            else:
                print('El Presio y Stok debe de ser positivo')
        else:
            print('El codigo ya existe')
        resp = input('Desea Continuar? (s/n): ')
    return productos
#--------------------------------------------------------------------------------------------
def mostrarProductos(x):
    for clave,valor in x.items():
        print(f'Codigo: {clave} Descripcion: {valor[0]} Precio: {valor[1]} Stock: {valor[2]}')  
#--------------------------------------------------------------------------------------------
def stock(x,desde,hasta):
    for clave,valor in x.items():
        if valor[2] >= desde and valor[2] <= hasta:
            print(f'Codigo: {clave} Descripcion: {valor[0]} Precio: {valor[1]} Stock: {valor[2]}') 
#--------------------------------------------------------------------------------------------
def incrementar(d,x,y):
    for clave,valor in d.items():
        if valor[2] < y:
            valor[2] = valor [2] + x
#--------------------------------------------------------------------------------------------
def verificarStock(x):
    aux = False
    for clave,valor in x.items():
        if valor[2] == 0:
            aux = True
    return aux
#--------------------------------------------------------------------------------------------
def eliminarStock(x):
    for clave,valor in list(x.items()):
        if valor[2] == 0:
            del x[clave]
#--------------------------------------------------------------------------------------------
opcion = 0
productos = {}
variablesIncializadas = False
while opcion != 6:
    opcion = menu()
    if opcion == 1:
        variablesIncializadas = True
        print('**REGISTRAR PRODUCTOS**')
        productos = registrarProductos(productos)
        esperar()
    elif opcion == 2:
        if variablesIncializadas:
            print('**PRODUCTOS**')
            mostrarProductos(productos)
        else:
            print('No hay Productos Cargados')
        esperar()
    elif opcion == 3:
        if variablesIncializadas:
            desde = int(input('Stock desde: '))
            hasta = int(input('Stock hasta: '))
            print(f'*Stock [{desde},{hasta}]*')
            stock(productos,desde,hasta)
        else:
            print('No hay Productos Cargados')
        esperar()
    elif opcion == 4:
        if variablesIncializadas:
            print('**INCREMENTAR STOCK**')
            x = int(input('Ingrese el Valor de X: '))
            y = int(input('Ingrese el Valor de Y: '))
            incrementar(productos,x,y)
            print('**STOCK ACTUALIZADO**')
            mostrarProductos(productos)
        else:
            print('No hay Productos Cargados')
        esperar()
    elif opcion == 5:
        if variablesIncializadas:
            if verificarStock(productos):
                eliminarStock(productos)
                print('Se eliminaron Correctamente')
            else:
                print('No se registraron productos con Stock = 0')
        else:
            print('No hay Productos Cargados')
        esperar()
print('Fin del Progarama')
