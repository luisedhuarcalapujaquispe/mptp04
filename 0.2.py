"""Mediante un menú de opciones realizar el siguiente programa modular para gestionar el listado de 
   notas de un examen para los alumnos de una institución educativa:
    a. Registrar alumnos: para cada uno se debe solicitar DNI, nombre y nota. Validar que la nota 
       se encuentre entre 0 y 10. El proceso finaliza cuando se ingresa un DNI igual a cero.
    b. Mostrar el listado de alumnos con sus respectivas notas. 
    c. Buscar un alumno por su DNI y mostrar su nombre y nota.
    d. Modificar los datos de un alumno buscando por DNI (el DNI no se puede modificar). 
    e. Eliminar un alumno buscando por DNI. Emitir un mensaje de confirmación. 
    f. Mostrar los alumnos que obtuvieron nota mayor o igual a una dada y el promedio 
       correspondiente.
    g. Salir"""
#--------------------------------------------------------------------------------------------
import os
#--------------------------------------------------------------------------------------------
def menu():
    os.system('cls')
    print("MENU PRINCIPAL \n" + 
        "1-Registrar Alumnos\n" +
        "2-Mostrar listado de Alumnos\n" +     
        "3-Buscar Alumno por DNI  \n" +
        "4-Modificar datos del Alumno por DNI \n" +
        "5-Eliminar Alumno por DNI \n" +
        "6-Promedio de notas \n" +
        "7-Salir")
    opcion = int(input("ELIGA UN OPCION: "))
    while not (opcion >= 1 and opcion <= 7):
        opcion = int(input("ELIGA UN OPCION: "))
    return opcion
#--------------------------------------------------------------------------------------------
def esperar():
    input('Presione una tecla...')
#--------------------------------------------------------------------------------------------
def validarNota():
    nota = float(input('Ingrese Nota: '))
    while not(nota >=0 and nota <=10):
        print('Nota no valida')
        nota = float(input('Ingrese Nota: '))
    return nota    
#--------------------------------------------------------------------------------------------
def ingresarAlumnos():
    lista = []
    respuesta = 's'
    while respuesta == 's':
        dni = int(input('DNI: '))
        nombre = input('Nombre: ')
        nota = validarNota()
        lista.append([dni, nombre, nota])
        respuesta = input('Desea Continuar? (s/n) ')
    return lista    
#--------------------------------------------------------------------------------------------
def buscarPorDni(alumnos, dni):
    pos = -1
    for (i,item) in enumerate(alumnos):
        if(item[0] == dni):
            pos = i
            break
    return pos
#--------------------------------------------------------------------------------------------
def buscarAlumno(alumnos):
    dni = int(input('Ingrese DNI: '))
    pos = buscarPorDni(alumnos, dni)
    if (pos == -1):
        print('No existe el DNI ingresado')
    else:
        print(alumnos[pos])
#--------------------------------------------------------------------------------------------
def modificarAlumno(alumnos):
    dni = int(input('DNI: '))
    pos = buscarPorDni(alumnos, dni)
    if (pos == -1):
        print('No existe el DNI ingresado')
    else:
        nombre = input('Nuevo Nombre: ')
        nota = float(input('Nueva Nota: '))
        alumnos[pos][1] = nombre
        alumnos[pos][2] = nota
        print('El Alumno fue Modificado')
#--------------------------------------------------------------------------------------------
def eliminarAlumno(alumnos):
    dni = int(input('DNI: '))
    pos = buscarPorDni(alumnos, dni)
    if (pos == -1):
        print('No existe el DNI ingresado')
    else: 
        alumnos.pop(pos)
        print('El Alumno fue Eliminado')  
#--------------------------------------------------------------------------------------------
def promedioNotas(alumnos):
    suma = 0
    for item in alumnos:
        suma += item[2]
    return suma / len(alumnos)
#--------------------------------------------------------------------------------------------
def superanPromedio(alumnos):
    promedio = promedioNotas(alumnos)
    for item in alumnos:
        if item[2] > promedio:
            print(item)
#--------------------------------------------------------------------------------------------
opcion = 0
alumnos = []
variablesIncializadas = False
while opcion != 7:
    opcion = menu()
    if opcion == 1:
        variablesIncializadas = True
        alumnos = ingresarAlumnos()
        esperar()
    elif opcion == 2:
        if variablesIncializadas:
            print(alumnos)
        else:
            print('No hay notas Ingresadas')
        esperar()
    elif opcion == 3:
        if variablesIncializadas:
            buscarAlumno(alumnos)
        else:
            print('No hay notas Ingresadas')
        esperar()
    elif opcion == 4:
        if variablesIncializadas:
            modificarAlumno(alumnos)
        else:
            print('No hay notas Ingresadas')
        esperar()       
    elif opcion == 5:
        if variablesIncializadas:
            eliminarAlumno(alumnos)
        else:
            print('No hay notas Ingresadas')
        esperar()
    elif opcion == 6:
        if variablesIncializadas:
            print('El Promedio: ',promedioNotas(alumnos))
            print('los alumnos que superan el promedio son:')
            superanPromedio(alumnos)
        else:
            print('No hay notas Ingresadas')
        esperar()
print('Fin del Programa')
